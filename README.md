# docker-tutorial

For using this repository as a container registry, build a docker image locally (with a Dockerfile present)

e.g.

```
docker build -t gitlab-registry.cern.ch/kati/docker-tutorial .
```

and then push it to this project's registry:

```
docker push gitlab-registry.cern.ch/kati/docker-tutorial
```

